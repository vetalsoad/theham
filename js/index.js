"use strict";

/* <!--  block 3 our services  --> */

const items = document.querySelectorAll(".block3-item");

const itemsArr = Array.prototype.slice.call(items);

// додаємо обробник подій на кожен елемент
itemsArr.forEach(function (item) {
  item.addEventListener("click", function () {
    // видаляємо клас "active" з усіх елементів списку
    itemsArr.forEach(function (item) {
      item.classList.remove("active");
    });

    // додаємо клас "active" на поточний елемент
    this.classList.add("active");

    // отримуємо ідентифікатор вкладки, пов'язаний з поточним елементом
    const tabId = this.getAttribute("data-tab");

    // отримуємо всі блоки з контентом вкладок
    const tabs = document.querySelectorAll(".block3-img-text");

    // перетворюємо список блоків в масив
    const tabsArr = Array.prototype.slice.call(tabs);

    // переключаємо видимість блоків контенту вкладок
    tabsArr.forEach(function (tab) {
      if (tab.getAttribute("data-tab") === tabId) {
        tab.style.display = "flex";
      } else {
        tab.style.display = "none";
      }
    });
  });
});

/* block 5 our amazing work */

const menuItems = document.querySelectorAll(".block5-item");

menuItems.forEach((item) => {
  item.addEventListener("click", () => {
    // видаляємо клас активного елемента для всіх елементів списку
    menuItems.forEach((item) => {
      item.classList.remove("block5-item-active");
    });
    // додаємо клас активного елемента до клікнутого елемента
    item.classList.add("block5-item-active");
  });
});

const imagesToAdd = [
  "./images/block5/start-images/img13.jpg",
  "./images/block5/start-images/img14.jpg",
  "./images/block5/start-images/img15.jpg",
  "./images/block5/start-images/img16.jpg",
  "./images/block5/start-images/img17.jpg",
  "./images/block5/start-images/img18.jpg",
  "./images/block5/start-images/img19.jpg",
  "./images/block5/start-images/img20.jpg",
  "./images/block5/start-images/img21.jpg",
  "./images/block5/start-images/img22.jpg",
  "./images/block5/start-images/img23.jpg",
  "./images/block5/start-images/img24.jpg",
];

const gallery = document.querySelector(".block5-gallery");
const loadMoreBtn = document.querySelector("#load-more");
let loadedImagesCount = 0;

loadMoreBtn.addEventListener("click", () => {
  const maxImagesCount = 24;
  const imagesToAddCount = Math.min(12, maxImagesCount - loadedImagesCount);

  if (imagesToAddCount === 0) {
    loadMoreBtn.style.display = "none";
    return;
  }

  for (let i = 0; i < imagesToAddCount; i++) {
    const image = document.createElement("img");
    image.src = imagesToAdd[loadedImagesCount];
    const galleryItem = document.createElement("div");
    galleryItem.classList.add("block5-gallery-item");
    galleryItem.appendChild(image);
    gallery.appendChild(galleryItem);
    loadedImagesCount++;
  }

  loadMoreBtn.style.display = "none";
});

const filterButtons = document.querySelectorAll(".block5-item");

filterButtons.forEach((button) => {
  button.addEventListener("click", () => {
    const category = button.dataset.category;
    console.log(category);

    if (category === "all") {
      gallery.querySelectorAll(".block5-gallery-item").forEach((item) => {
        item.style.display = "block";
      });
    } else {
      gallery.querySelectorAll(".block5-gallery-item").forEach((item) => {
        if (item.classList.contains(category)) {
          item.style.display = "block";
        } else {
          item.style.display = "none";
        }
      });
    }

    filterButtons.forEach((button) => {
      button.classList.remove("block5-item-active");
    });

    button.classList.add("block5-item-active");
  });
});

/* block 7 slider */

$(document).ready(function () {
  $(".slider").slick({
    arrows: true,
    slidesToShow: 4,
    infinite: true,
    centerMode: false,
    initialSlide: 2,
    asNavFor: ".slider-big",
  });
  $(".slider-big").slick({
    arrows: false,
    fade: true,
    initialSlide: 2,
    asNavFor: ".slider",
  });
});
